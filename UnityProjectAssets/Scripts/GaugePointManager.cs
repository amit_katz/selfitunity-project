﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GaugePointManager : MonoBehaviour {

    public LineRenderer PointPrefab;
    public Texture point;
    public float xWrap = 150f;

//    private List<GameObject> PointsDrawn = new List<GameObject>();
    private LineRenderer currentPoint;
    private int PointVertexIndex = 2;

    // Use this for initialization
    void Start () {
        addPoint(5f);
	}

    void OnGUI()
    {
        GameObject[] Points = GameObject.FindGameObjectsWithTag("Point");
        foreach (GameObject obj in Points)
        {
            Rect pos = obj.GetComponent<GUITexture>().pixelInset;
            if (pos.x > (-1 * xWrap + 25))
            {
                //obj.SetActive(true);
                obj.GetComponent<GUITexture>().pixelInset = new Rect(pos.x - 100f * Time.deltaTime, pos.y, 0, 0);
            }
            else
            {
                Destroy(obj);
            }
            
        }
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void addPoint(float Ypos)
    {
        GameObject newPoint = (GameObject)Instantiate(Resources.Load("Point"));
        //newPoint.SetActive(false);
        newPoint.transform.parent = transform;
        

        //Debug.Log("Mesh Size x: " + Osize.size.x + " y: " + Osize.size.y);

        newPoint.transform.localPosition = new Vector3(0f, 0f, 0.1f);
        newPoint.transform.localScale = new Vector3(0.01f, 0.05f, 1f);
        newPoint.GetComponent<GUITexture>().pixelInset = new Rect(xWrap, Ypos - 3, 0, 0);
        
    }
}
