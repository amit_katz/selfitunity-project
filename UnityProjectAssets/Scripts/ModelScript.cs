﻿using UnityEngine;
using System.Collections;

public class ModelScript : MonoBehaviour {

	public PlayerData playerData;

	private static ModelScript s_Instance= null;
	public static ModelScript instance {
		get {
			if (s_Instance == null) {
				s_Instance= FindObjectOfType(typeof(ModelScript)) as ModelScript;
				if (s_Instance== null)
				{
					GameObject modelGameObject= new GameObject();
					modelGameObject.AddComponent<ModelScript>();
					modelGameObject.name = "ModelGameObject";
					s_Instance= FindObjectOfType(typeof(ModelScript)) as ModelScript;
					ModelScript.instance.Init();
				}
			}
			return s_Instance;
		}
	}

	void Awake()
	{
		s_Instance = this;
		Init ();
	}

	public void Init() {
		playerData = Data2Script.LoadData ();
	}
}
