﻿using UnityEngine;
using System.Collections;

using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;

public class MyoHandController : MonoBehaviour
{
    public GameObject myo = null;

    private Pose _lastPose = Pose.Unknown;
    private Texture texture = null;
    private Vector2 cursorPos = Vector2.zero;
    private Vector3 baseHandPos;
    private GameObject HandGo;
    private GameObject FistGo;
    /// <summary>
	/// Gets the cursor position.
	/// </summary>
	/// <returns>The cursor position.</returns>
	public Vector2 GetCursorPos()
    {
        return cursorPos;
    }


    // Use this for initialization
    void Start () {
        baseHandPos = transform.position;
        HandGo = GameObject.Find("Hand");
        FistGo = GameObject.Find("FistHand");
        HandGo.SetActive(true);
        FistGo.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        // Access the ThalmicMyo component attached to the Myo object.
        ThalmicMyo thalmicMyo = myo.GetComponent<ThalmicMyo>();
        if (thalmicMyo == null)
        {
            return;
        }
        if (thalmicMyo.pose != _lastPose)
        {
            _lastPose = thalmicMyo.pose;

            if (thalmicMyo.pose == Pose.FingersSpread)
            {
                HandGo.SetActive(true);
                FistGo.SetActive(false);
                //HandGo.SetActive(false);
                //FistGo.SetActive(true);
                ExtendUnlockAndNotifyUserAction(thalmicMyo);
                Debug.Log("FingersSpread");
            }
            else if (thalmicMyo.pose == Pose.Fist)
            {
                HandGo.SetActive(false);
                FistGo.SetActive(true);
                ExtendUnlockAndNotifyUserAction(thalmicMyo);
                Debug.Log("Fist");
            }
            else if (thalmicMyo.pose == Pose.DoubleTap)
            {
                HandGo.SetActive(true);
                FistGo.SetActive(false);
                Instantiate(Resources.Load("BagObj"));
                Debug.Log("DoubleTap");
            }
            else
            {
                HandGo.SetActive(true);
                FistGo.SetActive(false);
                ExtendUnlockAndNotifyUserAction(thalmicMyo);
                Debug.Log("normalHandTexture");
            }
        }

        // Handle the position
        cursorPos = new Vector2(thalmicMyo.transform.forward.x, thalmicMyo.transform.forward.y);
        float magnifier = 20.0f;
        float axisBost = 7f;
        //gameObject.transform.position = new Vector3 (gameObject.transform.position.x, thalmicMyo.transform.forward.y, thalmicMyo.transform.forward.z);
        Vector3 myoPos = new Vector3(thalmicMyo.transform.forward.x * axisBost - 5.5f, thalmicMyo.transform.forward.y * 0.7f + 1.8f, thalmicMyo.transform.forward.z * axisBost - 4);
        transform.position = Vector3.MoveTowards(transform.position, myoPos, magnifier * Time.deltaTime);
    }


    // Extend the unlock if ThalmcHub's locking policy is standard, and notifies the given myo that a user action was
    // recognized.
    void ExtendUnlockAndNotifyUserAction(ThalmicMyo myo)
    {
        ThalmicHub hub = ThalmicHub.instance;

        if (hub.lockingPolicy == LockingPolicy.Standard)
        {
            myo.Unlock(UnlockType.Timed);
        }

        myo.NotifyUserAction();
    }

}
