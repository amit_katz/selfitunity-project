﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;
using stream_emg = Thalmic.Myo.stream_emg;

public class HandAndEMGcontroller : MonoBehaviour
{
    [Tooltip("GUI-texture used to display the color camera feed on the scene background.")]
    public GUITexture backgroundImage;

    [Tooltip("Camera that will be used to overlay the 3D-objects over the background.")]
    public Camera foregroundCamera;

    [Tooltip("Index of the player, tracked by this component. 0 means the 1st player, 1 - the 2nd one, 2 - the 3rd one, etc.")]
    public int playerIndex = 0;

    [Tooltip("Game object used to overlay the joints.")]
    public GameObject jointPrefab;

    [Tooltip("Line object used to overlay the bones.")]
    public LineRenderer linePrefab;

    [Tooltip("Myo device controller.")]
    public GameObject myo = null;

    //public float smoothFactor = 10f;

    //public GUIText debugText;
    
    // private internal parameters
    private GameObject [] joints;
    private LineRenderer[] lines = null;

    private bool emg_init = false;
    private sbyte[] EmgData;

    private Quaternion initialRotation = Quaternion.identity;
    private GUIStyle guiStyle = new GUIStyle();

    void Start()
    {
        KinectManager manager = KinectManager.Instance;
        EmgData = new sbyte[8];

        if (manager && manager.IsInitialized())
        {
            int jointsCount = manager.GetJointCount();
            joints = new GameObject[jointsCount];

            if (jointPrefab)
            {
                // array holding the skeleton joints

                for (int i = 0; i < jointsCount; i++)
                {

                    if (i == (int)KinectInterop.JointType.AnkleRight ||
                        i == (int)KinectInterop.JointType.WristRight ||
                        i == (int)KinectInterop.JointType.HandRight ||
                        i == (int)KinectInterop.JointType.ElbowRight ||
                        i == (int)KinectInterop.JointType.HandTipRight)
                    {
                        GameObject joint = new GameObject();
                        joint = Instantiate(jointPrefab) as GameObject;
                        joint.transform.parent = transform;
                        joint.name = ((KinectInterop.JointType)i).ToString();
                        joint.SetActive(false);
                        joints[i] = joint;
                    }
                }
            }

            // array holding the skeleton lines
            lines = new LineRenderer[joints.Length];

            //			if(linePrefab)
            //			{
            //				for(int i = 0; i < lines.Length; i++)
            //				{
            //					lines[i] = Instantiate(linePrefab) as LineRenderer;
            //					lines[i].transform.parent = transform;
            //					lines[i].gameObject.SetActive(false);
            //				}
            //			}
        }

        // always mirrored
        initialRotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
    }

    void OnGUI()
    {
    }

    void Update()
    {
        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized() && foregroundCamera)
        {
            //backgroundImage.renderer.material.mainTexture = manager.GetUsersClrTex();
            if (backgroundImage && (backgroundImage.texture == null))
            {
                backgroundImage.texture = manager.GetUsersClrTex();
            }

            // get the background rectangle (use the portrait background, if available)
            Rect backgroundRect = foregroundCamera.pixelRect;
            PortraitBackground portraitBack = PortraitBackground.Instance;

            if (portraitBack && portraitBack.enabled)
            {
                backgroundRect = portraitBack.GetBackgroundRect();
            }

            // overlay all joints in the skeleton
            if (manager.IsUserDetected())
            {
                long userId = manager.GetUserIdByIndex(playerIndex);
                int jointsCount = manager.GetJointCount();

                for (int i = 0; i < joints.Length; i++)
                {
                    int joint = i;

                    if (manager.IsJointTracked(userId, joint))
                    {
                        Vector3 posJoint = manager.GetJointPosColorOverlay(userId, joint, foregroundCamera, backgroundRect);
                        //Vector3 posJoint = manager.GetJointPosition(userId, joint);

                        if (joints[i] != null)
                        {
                            // overlay the joint
                            if (posJoint != Vector3.zero)
                            {
                                //								if(debugText && joint == 0)
                                //								{
                                //									debugText.GetComponent<GUIText>().text = string.Format("{0} - {1}\nRealPos: {2}", 
                                //									                                       (KinectInterop.JointType)joint, posJoint,
                                //									                                       manager.GetJointPosition(userId, joint));
                                //								}

                                joints[i].SetActive(true);
                                joints[i].transform.position = posJoint;

                                Quaternion rotJoint = manager.GetJointOrientation(userId, joint, false);
                                rotJoint = initialRotation * rotJoint;
                                joints[i].transform.rotation = rotJoint;
                            }
                            else
                            {
                                joints[i].SetActive(false);
                            }
                        }

                        if (lines[i] == null && linePrefab != null)
                        {
                            lines[i] = Instantiate(linePrefab) as LineRenderer;
                            lines[i].transform.parent = transform;
                            lines[i].gameObject.SetActive(false);
                        }

                        if (lines[i] != null && joints[i] != null)
                        {
                            // overlay the line to the parent joint
                            int jointParent = (int)manager.GetParentJoint((KinectInterop.JointType)joint);
                            Vector3 posParent = manager.GetJointPosColorOverlay(userId, jointParent, foregroundCamera, backgroundRect);

                            if (posJoint != Vector3.zero && posParent != Vector3.zero)
                            {
                                lines[i].gameObject.SetActive(true);

                                //lines[i].SetVertexCount(2);
                                lines[i].SetPosition(0, posParent);
                                lines[i].SetPosition(1, posJoint);
                            }
                            else
                            {
                                lines[i].gameObject.SetActive(false);
                            }
                        }

                    }
                    else
                    {
                        if (joints[i] != null)
                        {
                            joints[i].SetActive(false);
                        }

                        if (lines[i] != null)
                        {
                            lines[i].gameObject.SetActive(false);
                        }
                    }
                }

            }
        }
    }

}
