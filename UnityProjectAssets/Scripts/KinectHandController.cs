﻿using UnityEngine;
using System.Collections;

public class KinectHandController : MonoBehaviour
{

    public enum HandEventType : int
    {
        None = 0,
        Grip = 1,
        Release = 2
    }

    [Tooltip("Smooth factor for cursor movement.")]
    public float smoothFactor = 10f;

    [Tooltip("Index of the player, tracked by this component. 0 means the 1st player, 1 - the 2nd one, 2 - the 3rd one, etc.")]
    public int playerIndex = 0;

    [Tooltip("Number of bags on the conveyner.")]
    public int numOfBags = 8;

    private long primaryUserID = 0;

    private bool isLeftHandPrimary = false;
    private bool isRightHandPrimary = false;

    private bool isLeftHandPress = false;
    private bool isRightHandPress = false;

    private Vector3 cursorScreenPos = Vector3.zero;
    private bool dragInProgress = false;

    private KinectInterop.HandState leftHandState = KinectInterop.HandState.Unknown;
    private KinectInterop.HandState rightHandState = KinectInterop.HandState.Unknown;

    private HandEventType leftHandEvent = HandEventType.None;
    private HandEventType lastLeftHandEvent = HandEventType.Release;

    // Bool to keep track whether Kinect and Interaction library have been initialized
    private bool interactionInited = false;

    private Vector3 rightHandPos = Vector3.zero;
    private Vector3 rightHandScreenPos = Vector3.zero;
    private Vector3 rightIboxLeftBotBack = Vector3.zero;
    private Vector3 rightIboxRightTopFront = Vector3.zero;
    private bool isRightIboxValid = false;
    private bool isRightHandInteracting = false;
    private float rightHandInteractingSince = 0f;

    private HandEventType rightHandEvent = HandEventType.None;
    private HandEventType lastRightHandEvent = HandEventType.Release;

    private Vector3 leftHandPos = Vector3.zero;
    private Vector3 leftHandScreenPos = Vector3.zero;
    private Vector3 leftIboxLeftBotBack = Vector3.zero;
    private Vector3 leftIboxRightTopFront = Vector3.zero;
    private bool isleftIboxValid = false;
    private bool isLeftHandInteracting = false;
    private float leftHandInteractingSince = 0f;

    private Vector3 lastRightHandPos = Vector3.zero;
    private float lastRightHandTime = 0f;
    private bool isRightHandClick = false;
    private float rightHandClickProgress = 0f;

    private Vector3 lastLeftHandPos = Vector3.zero;
    private float lastLeftHandTime = 0f;
    private bool isLeftHandClick = false;
    private float leftHandClickProgress = 0f;

    private GameObject HandGo;
    private GameObject FistGo;

    private Transform Origin;
    private GameObject Dragged;

    // The single instance of FacetrackingManager
    private static KinectHandController instance;


    // GUI
    private GUIStyle guiStyle = new GUIStyle();
    private int gameScore;
    private int totalBags;

    /// <summary>
    /// Gets the single InteractionManager instance.
    /// </summary>
    /// <value>The InteractionManager instance.</value>
    public static KinectHandController Instance
    {
        get
        {
            return instance;
        }
    }

    /// <summary>
    /// Determines whether the InteractionManager was successfully initialized.
    /// </summary>
    /// <returns><c>true</c> if InteractionManager was successfully initialized; otherwise, <c>false</c>.</returns>
    public bool IsInteractionInited()
    {
        return interactionInited;
    }

    /// <summary>
    /// Gets the current user ID, or 0 if no user is currently tracked.
    /// </summary>
    /// <returns>The user ID</returns>
    public long GetUserID()
    {
        return primaryUserID;
    }

    /// <summary>
    /// Gets the current left hand event (none, grip or release).
    /// </summary>
    /// <returns>The current left hand event.</returns>
    public HandEventType GetLeftHandEvent()
    {
        return leftHandEvent;
    }

    /// <summary>
    /// Gets the last detected left hand event (grip or release).
    /// </summary>
    /// <returns>The last left hand event.</returns>
    public HandEventType GetLastLeftHandEvent()
    {
        return lastLeftHandEvent;
    }

    /// <summary>
    /// Gets the current normalized viewport position of the left hand, in range [0, 1].
    /// </summary>
    /// <returns>The left hand viewport position.</returns>
    public Vector3 GetLeftHandScreenPos()
    {
        return leftHandScreenPos;
    }

    /// <summary>
    /// Determines whether the left hand is primary for the user.
    /// </summary>
    /// <returns><c>true</c> if the left hand is primary for the user; otherwise, <c>false</c>.</returns>
    public bool IsLeftHandPrimary()
    {
        return isLeftHandPrimary;
    }

    /// <summary>
    /// Determines whether the left hand is pressing.
    /// </summary>
    /// <returns><c>true</c> if the left hand is pressing; otherwise, <c>false</c>.</returns>
    public bool IsLeftHandPress()
    {
        return isLeftHandPress;
    }

    /// <summary>
    /// Determines whether a left hand click is detected, false otherwise.
    /// </summary>
    /// <returns><c>true</c> if a left hand click is detected; otherwise, <c>false</c>.</returns>
    public bool IsLeftHandClickDetected()
    {
        if (isLeftHandClick)
        {
            isLeftHandClick = false;
            leftHandClickProgress = 0f;
            lastLeftHandPos = Vector3.zero;
            lastLeftHandTime = Time.realtimeSinceStartup;

            return true;
        }

        return false;
    }

    /// <summary>
    /// Gets the left hand click progress, in range [0, 1].
    /// </summary>
    /// <returns>The left hand click progress.</returns>
    public float GetLeftHandClickProgress()
    {
        return leftHandClickProgress;
    }

    /// <summary>
    /// Gets the current right hand event (none, grip or release).
    /// </summary>
    /// <returns>The current right hand event.</returns>
    public HandEventType GetRightHandEvent()
    {
        return rightHandEvent;
    }

    /// <summary>
    /// Gets the last detected right hand event (grip or release).
    /// </summary>
    /// <returns>The last right hand event.</returns>
    public HandEventType GetLastRightHandEvent()
    {
        return lastRightHandEvent;
    }

    /// <summary>
    /// Gets the current normalized viewport position of the right hand, in range [0, 1].
    /// </summary>
    /// <returns>The right hand viewport position.</returns>
    public Vector3 GetRightHandScreenPos()
    {
        return rightHandScreenPos;
    }

    /// <summary>
    /// Determines whether the right hand is primary for the user.
    /// </summary>
    /// <returns><c>true</c> if the right hand is primary for the user; otherwise, <c>false</c>.</returns>
    public bool IsRightHandPrimary()
    {
        return isRightHandPrimary;
    }

    /// <summary>
    /// Determines whether the right hand is pressing.
    /// </summary>
    /// <returns><c>true</c> if the right hand is pressing; otherwise, <c>false</c>.</returns>
    public bool IsRightHandPress()
    {
        return isRightHandPress;
    }

    /// <summary>
    /// Determines whether a right hand click is detected, false otherwise.
    /// </summary>
    /// <returns><c>true</c> if a right hand click is detected; otherwise, <c>false</c>.</returns>
    public bool IsRightHandClickDetected()
    {
        if (isRightHandClick)
        {
            isRightHandClick = false;
            rightHandClickProgress = 0f;
            lastRightHandPos = Vector3.zero;
            lastRightHandTime = Time.realtimeSinceStartup;

            return true;
        }

        return false;
    }

    /// <summary>
    /// Gets the right hand click progress, in range [0, 1].
    /// </summary>
    /// <returns>The right hand click progress.</returns>
    public float GetRightHandClickProgress()
    {
        return rightHandClickProgress;
    }

    /// <summary>
    /// Gets the current cursor normalized viewport position.
    /// </summary>
    /// <returns>The cursor viewport position.</returns>
    public Vector3 GetCursorPosition()
    {
        return cursorScreenPos;
    }


    //----------------------------------- end of public functions --------------------------------------//

    // Use this for initialization
    void Start () {
        instance = this;
        interactionInited = true;
        HandGo = GameObject.Find("Hand");
        FistGo = GameObject.Find("FistHand");
        HandGo.SetActive(false);
        FistGo.SetActive(false);
        gameScore = 0;
        totalBags = 0;
        ModelScript.instance.playerData.totalBags = totalBags;
        for (int i = 0; i < numOfBags; i++)
        {
            StartCoroutine(addBag(i+1));
        }
    }

    IEnumerator addBag(int d)
    {
        yield return new WaitForSeconds(4 * d);
        Instantiate(Resources.Load("BagObj"));
        ModelScript.instance.playerData.totalBags++;
    }

    void OnGUI()
    {
        guiStyle.fontSize = 18;
        guiStyle.normal.textColor = Color.red;
        gameScore = ModelScript.instance.playerData.LuggageScore;
        totalBags = ModelScript.instance.playerData.totalBags;
        GUI.Label(new Rect(20, 60, 200, 100), "Score: " + gameScore, guiStyle);
        GUI.Label(new Rect(20, 100, 200, 100), "Total Bags entered: " + totalBags, guiStyle);
    }

    void OnDestroy()
    {
        // uninitialize Kinect interaction
        if (interactionInited)
        {
            interactionInited = false;
            instance = null;
        }
    }

    // Update is called once per frame
    void Update () {
        KinectManager kinectManager = KinectManager.Instance;

        // update Kinect interaction
        if (kinectManager && kinectManager.IsInitialized())
        {
            primaryUserID = kinectManager.GetUserIdByIndex(playerIndex);
            if (primaryUserID != 0)
            {
                HandEventType handEvent = HandEventType.None;

                // get the right hand state
                rightHandState = kinectManager.GetRightHandState(primaryUserID);
                // check if the right hand is interacting
                isRightIboxValid = kinectManager.GetRightHandInteractionBox(primaryUserID, ref rightIboxLeftBotBack, ref rightIboxRightTopFront, isRightIboxValid);
                if (isRightIboxValid && //bRightHandPrimaryNow &&
                                   kinectManager.GetJointTrackingState(primaryUserID, (int)KinectInterop.JointType.HandRight) != KinectInterop.TrackingState.NotTracked)
                {
                    rightHandPos = kinectManager.GetJointPosition(primaryUserID, (int)KinectInterop.JointType.HandRight);

                    rightHandScreenPos.x = Mathf.Clamp01((rightHandPos.x - rightIboxLeftBotBack.x) / (rightIboxRightTopFront.x - rightIboxLeftBotBack.x));
                    rightHandScreenPos.y = Mathf.Clamp01((rightHandPos.y - rightIboxLeftBotBack.y) / (rightIboxRightTopFront.y - rightIboxLeftBotBack.y));
                    rightHandScreenPos.z = Mathf.Clamp01((rightIboxLeftBotBack.z - rightHandPos.z) / (rightIboxLeftBotBack.z - rightIboxRightTopFront.z));

                    bool wasRightHandInteracting = isRightHandInteracting;
                    isRightHandInteracting = (rightHandPos.x >= (rightIboxLeftBotBack.x - 0.5f)) && (rightHandPos.x <= (rightIboxRightTopFront.x + 1.0f)) &&
                        (rightHandPos.y >= (rightIboxLeftBotBack.y - 0.1f)) && (rightHandPos.y <= (rightIboxRightTopFront.y + 0.7f)) &&
                        (rightIboxLeftBotBack.z >= rightHandPos.z) && (rightIboxRightTopFront.z * 0.8f <= rightHandPos.z);

                    if (!wasRightHandInteracting && isRightHandInteracting)
                    {
                        rightHandInteractingSince = Time.realtimeSinceStartup;
                    }

                    if (isLeftHandInteracting && isRightHandInteracting)
                    {
                        if (rightHandInteractingSince <= leftHandInteractingSince)
                            isLeftHandInteracting = false;
                        else
                            isRightHandInteracting = false;
                    }
                    // check for right press
                    isRightHandPress = ((rightIboxRightTopFront.z - 0.1f) >= rightHandPos.z);

                    // check for right hand click
                    float fClickDist = (rightHandPos - lastRightHandPos).magnitude;

                    if (!dragInProgress && isRightHandInteracting &&
                       (fClickDist < KinectInterop.Constants.ClickMaxDistance))
                    {
                        if ((Time.realtimeSinceStartup - lastRightHandTime) >= KinectInterop.Constants.ClickStayDuration)
                        {
                            if (!isRightHandClick)
                            {
                                isRightHandClick = true;
                                rightHandClickProgress = 1f;
                            }
                        }
                        else
                        {
                            rightHandClickProgress = (Time.realtimeSinceStartup - lastRightHandTime) / KinectInterop.Constants.ClickStayDuration;
                        }
                    }
                    else
                    {
                        isRightHandClick = false;
                        rightHandClickProgress = 0f;
                        lastRightHandPos = rightHandPos;
                        lastRightHandTime = Time.realtimeSinceStartup;
                    }
                }
                else
                {
                    isRightHandInteracting = false;
                    isRightHandPress = false;
                }


                // process left hand
                handEvent = HandStateToEvent(leftHandState, lastLeftHandEvent);

                if ((isLeftHandInteracting != isLeftHandPrimary) || (isRightHandInteracting != isRightHandPrimary))
                {
                    if (dragInProgress)
                    {
                        dragInProgress = false;
                    }

                    lastLeftHandEvent = HandEventType.Release;
                    lastRightHandEvent = HandEventType.Release;
                }

                if (handEvent != lastLeftHandEvent)
                {
                    if (!dragInProgress && (handEvent == HandEventType.Grip))
                    {
                        dragInProgress = true;
                    }
                    else if (dragInProgress && (handEvent == HandEventType.Release))
                    {
                        dragInProgress = false;
                    }
                }

                leftHandEvent = handEvent;
                if (handEvent != HandEventType.None)
                {
                    lastLeftHandEvent = handEvent;
                }

                // if the hand is primary, set the cursor position
                if (isLeftHandInteracting)
                {
                    isLeftHandPrimary = true;

                    if ((leftHandClickProgress < 0.8f) /**&& !isLeftHandPress*/)
                    {
                        float smooth = smoothFactor * Time.deltaTime;
                        if (smooth == 0f) smooth = 1f;

                        cursorScreenPos = Vector3.Lerp(cursorScreenPos, leftHandScreenPos, smooth);
                    }

                }
                else
                {
                    isLeftHandPrimary = false;
                }

                // process right hand
                handEvent = HandStateToEvent(rightHandState, lastRightHandEvent);

                if (handEvent != lastRightHandEvent)
                {
                    if (!dragInProgress && (handEvent == HandEventType.Grip))
                    {
                        dragInProgress = true;                        
                    }
                    else if (dragInProgress && (handEvent == HandEventType.Release))
                    {                        
                        dragInProgress = false;
                    }
                }

                rightHandEvent = handEvent;
                if (handEvent != HandEventType.None)
                {
                    lastRightHandEvent = handEvent;
                }

                // if the hand is primary, set the cursor position
                if (isRightHandInteracting)
                {
                    isRightHandPrimary = true;

                    if ((rightHandClickProgress < 0.8f) /**&& !isRightHandPress*/)
                    {
                        float smooth = smoothFactor * Time.deltaTime;
                        if (smooth == 0f) smooth = 1f;

                        cursorScreenPos = Vector3.Lerp(cursorScreenPos, rightHandScreenPos, smooth);
                    }
                    //
                    if (Dragged == null)
                    {
                        if (lastLeftHandEvent == HandEventType.Grip || lastRightHandEvent == HandEventType.Grip)
                        {
                            HandGo.SetActive(false);
                            FistGo.SetActive(true);
                        }
                        else
                        {
                            HandGo.SetActive(true);
                            FistGo.SetActive(false);

                        }
                    }
                    float magnifier = 20.0f;
                    Vector3 Mypos = new Vector3((-rightHandPos.z * 12.0f) + 8.0f, (rightHandPos.y * 1.7f) + 1f, (-rightHandPos.x * 12.0f) - 2);
                    // transform.position = Mypos;
                    transform.position = Vector3.MoveTowards(transform.position, Mypos, magnifier * Time.deltaTime);
                    //Debug.Log("Pos X: " + Mypos.x + " Y:" + Mypos.y + " Z: " + Mypos.z);

                }
                else
                {
                    isRightHandPrimary = false;
                }


            }
            else
            {
                leftHandState = KinectInterop.HandState.NotTracked;
                rightHandState = KinectInterop.HandState.NotTracked;

                isLeftHandPrimary = false;
                isRightHandPrimary = false;

                isLeftHandPress = false;
                isRightHandPress = false;

                leftHandEvent = HandEventType.None;
                rightHandEvent = HandEventType.None;

                lastLeftHandEvent = HandEventType.Release;
                lastRightHandEvent = HandEventType.Release;

                if (dragInProgress)
                {
                    dragInProgress = false;
                }

            }
        }
        if (!dragInProgress)
        {  // Let go the Bag...
            if (Dragged != null)
            {
                Dragged.GetComponent<Rigidbody>().useGravity = true;
                Dragged.transform.parent = Origin;
                this.GetComponent<Collider>().enabled = true;  
                GameObject bagFist = Dragged.transform.Find("BagFistHand").gameObject;
                bagFist.SetActive(false);                
                FistGo.SetActive(true);
                Dragged = null;
            }
        }

    // ---End Of Update() ---
    }

    // converts hand state to hand event type
    public static HandEventType HandStateToEvent(KinectInterop.HandState handState, HandEventType lastEventType)
    {
        switch (handState)
        {
            case KinectInterop.HandState.Open:
                return HandEventType.Release;

            case KinectInterop.HandState.Closed:
            case KinectInterop.HandState.Lasso:
                return HandEventType.Grip;

            case KinectInterop.HandState.Unknown:
                return lastEventType;
        }

        return HandEventType.None;
    }

    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.tag == "Bag")
        {
            if (dragInProgress)
            {
                Dragged = obj.gameObject;
                Origin = obj.transform.parent;
                this.GetComponent<Collider>().enabled = false;
                FistGo.SetActive(false);                
                GameObject bagFist = obj.transform.Find("BagFistHand").gameObject;
                bagFist.SetActive(true);
                obj.GetComponent<Rigidbody>().freezeRotation = true;
                obj.GetComponent<Rigidbody>().useGravity = false;
                obj.transform.parent = this.transform;
            }
            Debug.Log("Collide bag");
        }
    }

    public void resetBoard()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Bag");
        foreach (GameObject go in gos)
        {
            Destroy(go);
        }
        for (int i = 0; i < numOfBags; i++)
        {
            StartCoroutine(addBag(i + 1));
        }
    }


    // End of namespace
}
