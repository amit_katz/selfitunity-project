﻿using UnityEngine;
using System.Collections;

public class BatTriggerAction : MonoBehaviour {
    public AudioClip pingSound;
    private AudioSource source;
    private int force = 7;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider obj)
    {
        Rigidbody ballRigidBody = obj.GetComponent<Rigidbody>();
        ballRigidBody.AddForce(new Vector2(-30 * force, 5 * force));
        source.PlayOneShot(pingSound);
        Debug.Log("Ball Hit");
    }

}
