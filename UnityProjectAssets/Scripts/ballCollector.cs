﻿using UnityEngine;
using System.Collections;

public class ballCollector : MonoBehaviour
{

    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.tag == "Ball")
        {
            Instantiate(Resources.Load("Ball"));
            Invoke("DestroyBall", 15);
            Destroy(obj);
        }
    }

    void DestroyBall()
    {
        GameObject mBall = GameObject.FindGameObjectWithTag("Ball");

        if (mBall != null)
            Destroy(mBall);
    }
}
