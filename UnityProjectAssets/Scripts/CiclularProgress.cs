﻿using UnityEngine;
using System.Collections;

public class CiclularProgress : MonoBehaviour {
	
	public int timeToComplete = 3;
	
	// Use this for initialization
	void Start () {
		//Use this to Start progress
		//StartCoroutine(RadialProgress(timeToComplete));
	}
    void OnGUI()
    {
        int score = ModelScript.instance.playerData.maxScore;
        float sc = (float)(score * 0.012);
        gameObject.GetComponent<Renderer>().material.SetFloat("_Progress",sc);
        Debug.Log("Score: " + score + " Progress: "  + sc);
    }
}