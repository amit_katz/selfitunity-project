﻿using UnityEngine;
using System.Collections;

using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;


public class MyoArmController : MonoBehaviour
{

    public GameObject myo = null;
    private Quaternion _antiYaw = Quaternion.identity;
   // private float _referenceRoll = 0.0f;
    private Pose _lastPose = Pose.Unknown;
    private Texture texture = null;

    [Tooltip("Hand-cursor texture for the hand-grip state.")]
    public Texture gripHandTexture;
    [Tooltip("Hand-cursor texture for the hand-release state.")]
    public Texture releaseHandTexture;
    [Tooltip("Hand-cursor texture for the non-tracked state.")]
    public Texture normalHandTexture;

    // current cursor position
    private Vector2 cursorPos = Vector2.zero;

    /// <summary>
	/// Gets the cursor position.
	/// </summary>
	/// <returns>The cursor position.</returns>
	public Vector2 GetCursorPos()
    {
        return cursorPos;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // Access the ThalmicMyo component attached to the Myo object.
        ThalmicMyo thalmicMyo = myo.GetComponent<ThalmicMyo>();
        if (thalmicMyo == null)
        {
            return;
        }
        if (thalmicMyo.pose != _lastPose)
        {
            _lastPose = thalmicMyo.pose;

            if (thalmicMyo.pose == Pose.FingersSpread)
            {
                texture = releaseHandTexture;

                ExtendUnlockAndNotifyUserAction(thalmicMyo);
                Debug.Log("FingersSpread");
            }
            else if (thalmicMyo.pose == Pose.Fist)
            {
                texture = gripHandTexture;

                ExtendUnlockAndNotifyUserAction(thalmicMyo);
                Debug.Log("Fist");
            }
            else if (thalmicMyo.pose == Pose.DoubleTap)
            {
                Instantiate(Resources.Load("BagObj"));
                Debug.Log("DoubleTap");
            }
            else
            {
                texture = normalHandTexture;

                ExtendUnlockAndNotifyUserAction(thalmicMyo);
                Debug.Log("normalHandTexture");
            }
        }

        // Handle the position
        cursorPos = new Vector2(thalmicMyo.transform.forward.x, thalmicMyo.transform.forward.y);

    }
    void OnGUI()
    {
        
        if (texture == null)
        {
            texture = normalHandTexture;
        }
        if ((cursorPos != Vector2.zero) && (texture != null))
        {
            //handCursor.transform.position = cursorScreenPos; // Vector3.Lerp(handCursor.transform.position, cursorScreenPos, 3 * Time.deltaTime);
            Rect rectTexture = new Rect(cursorPos.x * Screen.width - texture.width / 2, (1f - cursorPos.y) * Screen.height - texture.height / 2,
                                        texture.width, texture.height);
            GUI.DrawTexture(rectTexture, texture);
        }
        //Debug.Log("Position X: " + cursorPos.x + " Y: " + cursorPos.y );
    }

    // Extend the unlock if ThalmcHub's locking policy is standard, and notifies the given myo that a user action was
    // recognized.
    void ExtendUnlockAndNotifyUserAction(ThalmicMyo myo)
    {
        ThalmicHub hub = ThalmicHub.instance;

        if (hub.lockingPolicy == LockingPolicy.Standard)
        {
            myo.Unlock(UnlockType.Timed);
        }

        myo.NotifyUserAction();
    }
}
