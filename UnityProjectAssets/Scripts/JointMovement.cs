﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class JointMovement : MonoBehaviour {
    // Kinect intialization parts
    public GameObject BodySrcManager;
    public JointType TrackedJoint;
    private BodySourceManager bodyManager;
    private Body[] bodies;

    // General parameters to UI
    public int AngleFix = 1;
    public int movmentDuplicator = 1;
    public int miror = 1;
    public float rotationDamping = 3.0f;
    public bool needPosition = true;
    public bool needRotation = true;

    // Local vars
    private bool localPos = false;
    private float baseX = 0, baseY = 0, baseZ = 0;

    // Use this for initialization
    void Start () {
        if (BodySrcManager == null)
        {
            Debug.Log("Assign Game object with Body Source Manager");
        }
        else
        {
            bodyManager = BodySrcManager.GetComponent<BodySourceManager>();
        }

    }
	
	// Update is called once per frame
	void Update () {
        // float xValue = Input.GetAxis("Horizontal");
        // float yValue = Input.GetAxis("Vertical");
        if (bodyManager == null)
        {
            return;
        }
        bodies = bodyManager.GetData();

        if (bodies == null)
        {
            return;
        }
        foreach (var body in bodies)
        {
            if (body == null)
            {
                continue;
            }
            if (body.IsTracked)
            {
                if (needPosition)
                {
                    if (!localPos)
                    {
                        baseX = gameObject.transform.position.x;
                        baseY = gameObject.transform.position.y;
                        baseZ = gameObject.transform.position.z;
                        localPos = true;
                    }

                    var pos = body.Joints[TrackedJoint].Position;
                    gameObject.transform.position = new Vector3((pos.X * movmentDuplicator) + baseX,
                                                                0,//pos.Y + baseY,
                                                                (pos.Z * movmentDuplicator) + baseZ);
                    Debug.Log("Pos X: " + pos.X + " Y: " + pos.Y + " Z: " + pos.Z);
                }

                if (needRotation)
                {
                    Windows.Kinect.Vector4 rotation = body.JointOrientations[TrackedJoint].Orientation;
                    gameObject.transform.localRotation =
                                Quaternion.Slerp(gameObject.transform.localRotation,
                                new Quaternion(rotation.X, rotation.Y * miror, rotation.Z, rotation.W), Time.time * 0.1f);
                }
                
            }
        }
    }
}
