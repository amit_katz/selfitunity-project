﻿using UnityEngine;
using System.Collections;
using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;
using stream_emg = Thalmic.Myo.stream_emg;


public class MyoHandPongController : MonoBehaviour
{
    public float batForce = 20.0f;
    private float batMagnifier = 1.0f;

    private enum GameStates { BeforeGame, Playing, AfterHit, GameOver };
    private GameStates mCurrentState = GameStates.BeforeGame;
    private bool isCaliberated = false;
    public GameObject myo = null;
    //private Vector3 batPos;
    private Vector3 vCurrPos;
    private Quaternion BaseRot, OpRot;
    public GameObject mPlayerGameObject;
    private Vector3 ballLocation;
    private GUIStyle guiStyle = new GUIStyle();
 
    private float roll = 0.0f;
    private int direction;
    private bool inCliberate = false;
    private int CliberateStep = 0;
    private Pose _lastPose = Pose.Unknown;
    private Vector3 CL, FL, CR, FR;
    private float rollCL, rollFL, rollCR, rollFR;
    private bool emg_init = false;
    private sbyte[] EmgData;

    //
    private float rollCenter = 42.0f;
    //private float BoardX = 3.0f;
    //private float BoardY = 4.0f;
    
    public Texture textureToDisplay;
    public GaugePointManager gaugeScr;

    // Use this for initialization
    void Start()
    {        
        //batPos = mPlayerGameObject.transform.position;
        BaseRot = mPlayerGameObject.transform.rotation;
        OpRot = new Quaternion(-BaseRot.x, -BaseRot.y, BaseRot.z, BaseRot.w);
        EmgData = new sbyte[8];
        // mBall = GameObject.Find("Ball");
        // ballLocation = mBall.transform.position;
    
    }

    void OnGUI()
    {
        guiStyle.fontSize = 16;
        guiStyle.normal.textColor = Color.blue;

        GUI.Label(new Rect(20, 70, 500, 100), "MYO Axis - X: "  + vCurrPos.x.ToString("F2") + " Y: " + vCurrPos.y.ToString("F2") + " Z: " + vCurrPos.z.ToString("F2"));
        GUI.Label(new Rect(20, 100, 400, 100), "MYO Roll: " + roll.ToString("F2"));
        GUI.Label(new Rect(20, 130, 400, 100), "MYO XDirection: " + direction);

        int eData = EmgData[0];
        
        gaugeScr.addPoint(((float)eData) /9f);
        Debug.Log("EMG1=" + eData);
        
        //GUI.Box(new Rect(900, 100, 400, 80), "EMG1");
        // GUI.DrawTexture()

        if (!isCaliberated && !inCliberate)
        {           
            guiStyle.normal.textColor = Color.blue;
            GUI.Label(new Rect(40, 20, 500, 100), "You need to cliberate first the Myo, press C to start clibration.", guiStyle);
        }

        if (inCliberate) 
        {
            string clibmsg = "Move to Closer Left point and tap fingers when ready";
            if (CliberateStep >= 0)
            {
                clibmsg = "Move to Far Left point and tap fingers when ready";                
                GUI.Label(new Rect(10, 400, 400, 100), "CL X: " + CL.x.ToString("F2") + " Y: " + CL.y.ToString("F2") + " Z: " + CL.z.ToString("F2") + " R: " + rollCL.ToString("F2"));
            }
            if (CliberateStep >= 1)
            {
                clibmsg = "Move to Closer Right point and tap fingers when ready";
                GUI.Label(new Rect(10, 350, 400, 100), "FL X: " + FL.x.ToString("F2") + " Y: " + FL.y.ToString("F2") + " Z: " + FL.z.ToString("F2") + " R: " + rollFL.ToString("F2"));
            }
            if (CliberateStep >= 2)
            {               
                clibmsg = "Move to Far Right point and tap fingers when ready";
                GUI.Label(new Rect(340, 400, 400, 100), "CR X: " + CR.x.ToString("F2") + " Y: " + CR.y.ToString("F2") + " Z: " + CR.z.ToString("F2") + " R: " + rollCR.ToString("F2"));
            }
            if (CliberateStep >= 3)
            {  
                clibmsg = "Press Escape to leave clibration";
                GUI.Label(new Rect(340, 350, 400, 100), "FR X: " + FR.x.ToString("F2") + " Y: " + FR.y.ToString("F2") + " Z: " + FR.z.ToString("F2") + " R: " + rollFR.ToString("F2"));
                isCaliberated = true;
            }
            rollCenter = (rollCL + rollCR) / 2;
           
            Debug.Log("batMagnifier = " + batMagnifier);
            GUI.Label(new Rect(10, 300, 500, 100), clibmsg, guiStyle);
        }      
    }

    // Update is called once per frame
    void Update()
    {
        ThalmicMyo thalmicMyo = myo.GetComponent<ThalmicMyo>();
        if (thalmicMyo == null)
        {
            Debug.Log("No Myo detected");
            return;
        }
        if (!emg_init)
        {
            thalmicMyo.EnableEMG(stream_emg.emg_enabled);
            emg_init = true;
            Debug.Log("EMG enabled");
        }

        if (emg_init)
        {
            EmgData = thalmicMyo.emgData;
            string st = "";
            for (int i = 0; i < 8; i++)
            {
                st += " " + i + ": " + EmgData[i];
            }
            //Debug.Log("EMG" + st);
        }
        vCurrPos = new Vector3(thalmicMyo.transform.forward.x, thalmicMyo.transform.forward.y, thalmicMyo.transform.forward.z);
        direction = (int)thalmicMyo.xDirection;
        
        Vector3 zeroRoll = computeZeroRollVector(myo.transform.forward);
        roll = rollFromZero(zeroRoll, myo.transform.forward, myo.transform.up) * -1;
        //Vector3 objectPos;
        //Debug.Log("MYO X: " + thalmicMyo.transform.forward.x + " Y: " + thalmicMyo.transform.forward.z);
        if (roll > rollCenter)
        { // Right pose
            mPlayerGameObject.transform.rotation = OpRot;
           
           // objectPos = new Vector3(reflectMove(thalmicMyo.transform.forward.x, CR.x, FR.x, BoardX), mPlayerGameObject.transform.position.y , reflectMove(thalmicMyo.transform.forward.z, CR.z, FR.z, BoardY));
        }
        else
        { // Left pose
            mPlayerGameObject.transform.rotation = BaseRot;
                        
            //objectPos = new Vector3(reflectMove(thalmicMyo.transform.forward.x, CL.x, FL.x,  BoardX), mPlayerGameObject.transform.position.y , reflectMove(thalmicMyo.transform.forward.z, CL.z, FL.z, BoardY) );
        }

        // if (isCaliberated)
        // {
        //Debug.Log("POS X: " + objectPos.x + " Y: " + objectPos.z);
        //mPlayerGameObject.transform.position = objectPos;
        float axisFix = 1;
        if (CL.x > FL.x)
            axisFix = -1;
        Vector3 myoPos = new Vector3(thalmicMyo.transform.forward.x * axisFix, thalmicMyo.transform.forward.y * -0.3f - 2.2f, thalmicMyo.transform.forward.z * axisFix);
        //transform.position = Vector3.MoveTowards(transform.position, -thalmicMyo.transform.forward, batMagnifier * batForce * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, -myoPos, batMagnifier * batForce * Time.deltaTime);
       //transform.position = new Vector3(transform.position.x, batPos.y, transform.position.z);
       // }

        if (Input.GetKey(KeyCode.C) && !inCliberate)
        {
            inCliberate = true;
            Debug.Log("Calibrate start");
            CliberateStep = -1;
        }

        if (Input.GetKey(KeyCode.Escape) && inCliberate)
        {
            inCliberate = false;
            CliberateStep = -1;
        }

        if (thalmicMyo.pose != _lastPose)
        {
            _lastPose = thalmicMyo.pose;
            if (thalmicMyo.pose == Pose.DoubleTap && inCliberate)
            {
                CliberateStep++;
                Debug.Log("Calibrate step = " + CliberateStep);
                if (CliberateStep == 0)
                {
                    CL.x = vCurrPos.x;
                    CL.y = vCurrPos.y;
                    CL.z = vCurrPos.z;
                    rollCL = roll;
                }
                if (CliberateStep == 1)
                {
                    FL.x = vCurrPos.x;
                    FL.y = vCurrPos.y;
                    FL.z = vCurrPos.z;
                    rollFR = roll;
                    batMagnifier += Mathf.Abs(CL.x - FL.x);
                }
                if (CliberateStep == 2)
                {
                    CR.x = vCurrPos.x;
                    CR.y = vCurrPos.y;
                    CR.z = vCurrPos.z;
                    rollCR = roll;
                }
                if (CliberateStep == 3)
                {
                    FR.x = vCurrPos.x;
                    FR.y = vCurrPos.y;
                    FR.z = vCurrPos.z;
                    rollFR = roll;
                }

            }
            ExtendUnlockAndNotifyUserAction(thalmicMyo);
        }
        

        if (Input.GetKeyUp(KeyCode.S))
        {
            onStartPress();
        }
    }

   /* private float reflectMove(float rm, float C, float F, float boardS)
    {
        if (Mathf.Abs(rm) < Mathf.Abs(C))
            rm = C;
        if (Mathf.Abs(rm) > Mathf.Abs(F))
            rm = F;
        float gap = Mathf.Abs(C - F);
        float fMove = (gap / 2) + C;
        float res = (rm - fMove) * (boardS / gap);

        Debug.Log("Calculated: " + res);
        return res;
    }
*/
    public void onStartPress()
    {
        Debug.Log ("On key pressed");
        if (mCurrentState == GameStates.BeforeGame && isCaliberated)
        {
            //FireBall();
            mCurrentState = GameStates.Playing;
            Debug.Log("GameStates.Playing");
        }

        GameObject mBall = GameObject.FindGameObjectWithTag("Ball");

        if (mBall != null)
            Destroy(mBall);

        Instantiate(Resources.Load("Ball"));
    }

    Vector3 computeZeroRollVector(Vector3 forward)
    {
        Vector3 antigravity = Vector3.up;
        Vector3 m = Vector3.Cross(myo.transform.forward, antigravity);
        Vector3 roll = Vector3.Cross(m, myo.transform.forward);

        return roll.normalized;
    }

    float rollFromZero(Vector3 zeroRoll, Vector3 forward, Vector3 up)
    {
        // The cosine of the angle between the up vector and the zero roll vector. Since both are
        // orthogonal to the forward vector, this tells us how far the Myo has been turned around the
        // forward axis relative to the zero roll vector, but we need to determine separately whether the
        // Myo has been rolled clockwise or counterclockwise.
        float cosine = Vector3.Dot(up, zeroRoll);

        // To determine the sign of the roll, we take the cross product of the up vector and the zero
        // roll vector. This cross product will either be the same or opposite direction as the forward
        // vector depending on whether up is clockwise or counter-clockwise from zero roll.
        // Thus the sign of the dot product of forward and it yields the sign of our roll value.
        Vector3 cp = Vector3.Cross(up, zeroRoll);
        float directionCosine = Vector3.Dot(forward, cp);
        float sign = directionCosine < 0.0f ? 1.0f : -1.0f;

        // Return the angle of roll (in degrees) from the cosine and the sign.
        return sign * Mathf.Rad2Deg * Mathf.Acos(cosine);
    }

    // Extend the unlock if ThalmcHub's locking policy is standard, and notifies the given myo that a user action was
    // recognized.
    void ExtendUnlockAndNotifyUserAction(ThalmicMyo myo)
    {
        ThalmicHub hub = ThalmicHub.instance;

        if (hub.lockingPolicy == LockingPolicy.Standard)
        {
            myo.Unlock(UnlockType.Timed);
        }

        myo.NotifyUserAction();
    }


}
