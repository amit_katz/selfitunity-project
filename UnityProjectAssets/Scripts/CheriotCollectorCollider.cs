﻿using UnityEngine;
using System.Collections;

public class CheriotCollectorCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerEnter(Collider obj)
    {
        if (obj.gameObject.tag == "Bag")
        {
            Debug.Log("Bag on chriot");
            StartCoroutine(LateDestroy(obj.gameObject));
            ModelScript.instance.playerData.LuggageScore++;
            Instantiate(Resources.Load("BagObj"));            
            ModelScript.instance.playerData.totalBags++;
        }
    }

    IEnumerator LateDestroy (GameObject ob)
    {
        Debug.Log("Destroy on chriot");
        
        yield return new WaitForSeconds(5);
        Destroy(ob);
    }
}
