﻿using UnityEngine;

[System.Serializable]
public class PlayerData: System.Object{
	public int headAngle;
	public int bodyAngle;
    public int score;
	public int maxScore;
    public bool needMooving;
    public int LuggageScore;
    public int totalBags;
}
