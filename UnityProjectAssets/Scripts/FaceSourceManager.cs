﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;
using Microsoft.Kinect.Face;

public class FaceSourceManager : MonoBehaviour
{
    public GameObject BodySrcManager;
    private KinectSensor kinectSensor;
    private BodySourceManager bodyManager;
    private int bodyCount;
    private int firstTrackedBody = -1;
    private Body[] bodies = null;
    private FaceFrameSource[] faceFrameSources = null;
    private FaceFrameReader[] faceFrameReaders = null;
    public JointType TrackedJoint;
    float currentYValue = 0;
    public int YfixValue = 0;
    //private float yAxisFix = 0;
    
    private int rCounter = 0;
    private bool faceTracked = false;
    public int angleMult = 10;
    public float rotationDamping = 3.0f;
    private bool TraceYaxis = false;
    private GUIStyle guiStyle = new GUIStyle();

    void Start()
    {
        // one sensor is currently supported
        kinectSensor = KinectSensor.GetDefault();

        if (BodySrcManager == null)
        {
            Debug.Log("Assign Game object with Body Source Manager");
        }
        else
        {
            bodyManager = BodySrcManager.GetComponent<BodySourceManager>();
        }

        // set the maximum number of bodies that would be tracked by Kinect
        bodyCount = kinectSensor.BodyFrameSource.BodyCount;

        // allocate storage to store body objects
        bodies = new Body[bodyCount];

        // specify the required face frame results
        FaceFrameFeatures faceFrameFeatures =
                FaceFrameFeatures.RotationOrientation;

        // create a face frame source + reader to track each face in the FOV
        faceFrameSources = new FaceFrameSource[bodyCount];
        faceFrameReaders = new FaceFrameReader[bodyCount];
        for (int i = 0; i < bodyCount; i++)
        {
            // create the face frame source with the required face frame features and an initial tracking Id of 0
            faceFrameSources[i] = FaceFrameSource.Create(kinectSensor, 0, faceFrameFeatures);

            // open the corresponding reader
            faceFrameReaders[i] = faceFrameSources[i].OpenReader();
        }
    }

    void OnGUI()
    {
        guiStyle.fontSize = 18;
        int Yaxis = (int)currentYValue;// (gameObject.transform.rotation.y /  Mathf.PI * 10.0);
        if (TrackedJoint != JointType.Head)
        {
            ModelScript.instance.playerData.bodyAngle = Yaxis;
            GUI.Label(new Rect(20, 800 + YfixValue, 200, 100), "Body Y Angle: " + Yaxis);
        }
        else
        {
            ModelScript.instance.playerData.headAngle = Yaxis;
            GUI.Label(new Rect(20, 800, 200, 100), "Head Y Angle: " + Yaxis);
        }
        int score = Mathf.Abs(ModelScript.instance.playerData.bodyAngle - ModelScript.instance.playerData.headAngle);
        ModelScript.instance.playerData.score = score;
        int MaxScore = ModelScript.instance.playerData.maxScore;
        
        GUI.Label(new Rect(20, 120, 200, 100), "Max Score: " + MaxScore, guiStyle);
        //Debug.Log("Score: " + score);
        //gameObject.GetComponent<Renderer>().material.SetFloat("_Progress", score);
        if (faceTracked)
        {
            GUI.Label(new Rect(20, 150, 200, 100), "Face Detected!", guiStyle);
            if (MaxScore < score)
            {
                MaxScore = score;
                ModelScript.instance.playerData.maxScore = MaxScore;
            }
        }
    }

    void Update()
    {
        rCounter = 0;            
        bodies = bodyManager.GetData();
        
        if (bodies == null)
        {
            return;
        }
        if (TrackedJoint != JointType.Head)
        {
            foreach (var body in bodies)
            {                
                if (body == null)
                {
                    continue;
                }
                if (body.IsTracked)
                {
                    if (firstTrackedBody == -1)
                    {
                        firstTrackedBody = rCounter;
                    }
                    if (rCounter == firstTrackedBody)
                    {
                        //var pos = body.Joints[TrackedJoint].Position;
                        Windows.Kinect.Vector4 rotation = body.JointOrientations[TrackedJoint].Orientation;                        
                        gameObject.transform.localRotation = Quaternion.Slerp(gameObject.transform.localRotation,
                                                             new Quaternion(rotation.X * 0, rotation.Y * -1, rotation.Z, rotation.W), Time.time * 0.1f);

                        currentYValue = gameObject.transform.eulerAngles.y - 180;
                        Debug.Log("Body Direction: "  + currentYValue + " Player ID: " + firstTrackedBody);
                    }
                }
                rCounter++;
            }
        }
        else
        {
            // iterate through each body and update face source
            for (int i = 0; i < bodyCount; i++)
            {
                // check if a valid face is tracked in this face source				
                if (faceFrameSources[i].IsTrackingIdValid)
                {
                    using (FaceFrame frame = faceFrameReaders[i].AcquireLatestFrame())
                    {
                        if (frame != null)
                        {

                            if (firstTrackedBody == -1)
                            {
                                firstTrackedBody = i;
                            }
                            if (frame.TrackingId == 0 || firstTrackedBody != i)
                            {
                                continue;
                            }
                            if (TraceYaxis)
                            {
                                // do something with result
                                var result = frame.FaceFrameResult;
                                Windows.Kinect.Vector4 rotation = result.FaceRotationQuaternion;
                                float rot = rotation.Y * (-1) * angleMult;
                                currentYValue = Mathf.LerpAngle(currentYValue, rot, rotationDamping * Time.deltaTime);                            
                                gameObject.transform.eulerAngles = new Vector3(0, currentYValue, 0);                                
                                Vector3 forward = Camera.main.transform.forward * 500;
                                Debug.DrawRay(gameObject.transform.position, forward, UnityEngine.Color.red, 1, true);
                                // Debug.Log("Direction: " + forward.x + " " + forward.y );
                                //Debug.Log("Player ID: " + firstTrackedBody + " Face Angle: " + currentYValue);
                            }                            
                        }
                    }
                }
                else
                {
                    // check if the corresponding body is tracked 
                    if (bodies[i].IsTracked && !faceTracked)
                    {
                        // update the face frame source to track this body
                        faceFrameSources[i].TrackingId = bodies[i].TrackingId;
                        faceTracked = true;
                    }
                }
            }
        }
        
    }

    public void StartY() {
        //GameObject btn = GameObject.Find("Button");
        GameObject text = GameObject.Find("StartText");
        if (!TraceYaxis)
        {
            TraceYaxis = true;
            text.SetActive(false);
        }
        
    }

    public void ResetScore()
    {
        ModelScript.instance.playerData.maxScore = 0;
    }
    
}