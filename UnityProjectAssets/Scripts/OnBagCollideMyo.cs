﻿using UnityEngine;
using System.Collections;

using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;


public class OnBagCollideMyo : MonoBehaviour {
    public GameObject myo = null;

    private GameObject Hand;
    private bool InHold = false;

    // Use this for initialization
    void Start () {
	
	}

    void Update()
    {
        if (InHold)
        {
            transform.position = Hand.transform.position;
            this.GetComponent<Rigidbody>().useGravity = false;
        }
        else
        {
            this.GetComponent<Rigidbody>().useGravity = true;
        }
    }

    void OnTriggerEnter(Collider obj)
    {       
        if (obj.gameObject.name == "Hand" || obj.gameObject.name == "FistHand" )
        {
            Debug.Log("Hand/Fist collide bag");
            InHold = true;
            Hand = obj.gameObject;
            // Access the ThalmicMyo component attached to the Myo object.
            ThalmicMyo thalmicMyo = myo.GetComponent<ThalmicMyo>();
            if (thalmicMyo == null)
            {
                return;
            }
            if (thalmicMyo.pose == Pose.Fist)
            {
               
            }
            if (thalmicMyo.pose == Pose.FingersSpread)
            {
                InHold = false;
            }

        }
    }
}
