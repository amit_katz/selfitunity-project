﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class DetectMovement : MonoBehaviour {

    public GameObject BodySrcManager;
    public JointType TrackedJoint;
    private BodySourceManager bodyManager;
    private KinectSensor kinectSensor;
    private Body[] bodies = null;
    private int bodyCount;
    //private float currentXValue = 0;
    private float currentYValue = 0;
    public int angleMult = 10;
    public float rotationDamping = 3.0f;

    // Use this for initialization
    void Start () {
        // one sensor is currently supported
        kinectSensor = KinectSensor.GetDefault();
        if (BodySrcManager == null)
        {
            Debug.Log("Assign Game object with Body Source Manager");
        }
        else
        {
            bodyManager = BodySrcManager.GetComponent<BodySourceManager>();
        }
        // set the maximum number of bodies that would be tracked by Kinect
        bodyCount = kinectSensor.BodyFrameSource.BodyCount;

        // allocate storage to store body objects
        bodies = new Body[bodyCount];
    }
   
    // Update is called once per frame
    void Update()
    {
        // float xValue = Input.GetAxis("Horizontal");
        // float yValue = Input.GetAxis("Vertical");
         if (bodyManager == null)
         {
             return;
         }
         bodies = bodyManager.GetData();

         if (bodies == null)
         {
             return;
         }
         foreach (var body in bodies)
         {
             if (body == null)
             {
                 continue;
             }
             if (body.IsTracked)
             {
                //var pos = body.Joints[TrackedJoint].Position;
                var rotation = body.JointOrientations[TrackedJoint].Orientation;
                float rot = rotation.Y * (-1) * angleMult;
                //float yaw = Mathf.Asin(2 * ((rot.W * rot.Y) - (rot.X * rot.Z))); // Mathf.PI * 180.0;
                currentYValue = Mathf.LerpAngle(currentYValue, rot, rotationDamping * Time.deltaTime);
                //gameObject.transform.position = new Vector3(pos.X, pos.Y, 0);
                gameObject.transform.eulerAngles = new Vector3(0, currentYValue, 0);
                
                    //Quaternion.Euler(0, this.gameObject.transform.rotation.y + rotation.Y * -100, 0);
                Debug.Log("Head Direction: " + currentYValue + " " + rotation.Y + " " + Time.deltaTime);
            }
        }
    }



// End
}
