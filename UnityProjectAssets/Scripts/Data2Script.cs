﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Data2Script : MonoBehaviour {
	public static void SaveData(PlayerData playerData)
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath+ "/playerInfo.dat");
		bf.Serialize(file, playerData);
		file.Close();
	}
	public static PlayerData LoadData() {
		PlayerData playerData= new PlayerData();
		if (File.Exists(Application.persistentDataPath+ "/playerInfo.dat")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath+ "/playerInfo.dat", FileMode.Open);
			playerData= (PlayerData)bf.Deserialize(file);
			file.Close();
		}
		return playerData;
	}
}
